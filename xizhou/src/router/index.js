import Vue from 'vue'
import VueRouter from 'vue-router'

// 解决重复点击面包屑导航报错
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

import Login from '@/views/login/Login'
import Home from '@/views/home/Home'
import Layout from '@/views/layout/Layout'
import AccountList from '@/views/accountManagement/AccountList'
import AddAccount from '@/views/accountManagement/AddAccount'
import AddBed from '@/views/bedManagement/AddBed'
import IntegratedManagement from '@/views/bedManagement/IntegratedManagement'
import AdministrationRegister from '@/views/dynamicPersonal/AdministrationRegister'
import ComprehensiveList from '@/views/gradedDoctor/ComprehensiveList'
import ModifyPersonal from '@/views/gradedDoctor/ModifyPersonal'
import WorkforceManagement from '@/views/gradedDoctor/WorkforceManagement'
import Comprehensive from '@/views/gradedNurse/Comprehensive'
import Modify from '@/views/gradedNurse/Modify'
import Work from '@/views/gradedNurse/Work'
import RoomAdd from '@/views/hospitalGeneral/RoomAdd'
import RoomAdministration from '@/views/hospitalGeneral/RoomAdministration'
import AddDrugs from '@/views/medicalDrugs/AddDrugs'
import Integrated from '@/views/medicalDrugs/Integrated'
import PrescriptionAdministration from '@/views/medicalFront/PrescriptionAdministration'
import PrescriptionLssued from '@/views/medicalFront/PrescriptionLssued'
import Consume from '@/views/PatientData/Consume'
import Disease from '@/views/PatientData/Disease'
import Prescription from '@/views/pharmacy/Prescription'
import RevenueDataForm from '@/views/pharmacy/RevenueDataForm'

Vue.use(VueRouter)

const routes = [
  {
    path:'/',
    redirect: '/login',
  },
  {
    path: '/login',
    component: Login
  },
  {
    path:'/home',
    component:Layout,
    children:[
      { path:'/',component:Home } //没有三级路由是path留空即可
    ]
  },
  {
    path: '/accountManagement',
    component: Layout,
    meta: {title: '全账号管理账号'},
    redirect: '/accountManagement/AccountEdit',
    children: [
      {
        path: '/accountManagement/AccountList', component: AccountList,
        meta: {title: '账号管理'}
      },
      {
        path: '/accountManagement/AddAccount', component: AddAccount,
        meta: {title: '添加账号'}
      },
    ]
  },
  {
    path: '/bedManagement',
    component: Layout,
    meta: {title: '病床管理'},
    redirect: '/bedManagement/AddBed',
    children: [
      {
        path: '/bedManagement/AddBed', component: AddBed,
        meta: {title: '添加床位'},
      },
      {
        path: '/bedManagement/IntegratedManagement', component: IntegratedManagement,
        meta: {title: '综合管理'},
      },

    ]
  },
  {
    path: '/dynamicPersonal',
    component: Layout,
    meta: {title: '动态个人'},
    redirect: '/dynamicPersonal/AdministrationRegister',
    children: [
      {
        path: '/dynamicPersonal/AdministrationRegister', component: AdministrationRegister ,
        meta: {title: '挂号信息   '},
      }
    ]
  },
  {
    path: '/gradedDoctor',
    meta: {title: '分级医生'},
    component: Layout,
    redirect: '/gradedDoctor/ComprehensiveList',
    children: [
      {
        path: '/gradedDoctor/ComprehensiveList', component: ComprehensiveList ,
        meta: {title: '综合列表'},
      },
      {
        path: '/gradedDoctor/ModifyPersonal', component: ModifyPersonal,
        meta: {title: '修改个人信息'},
      },
      {
        path: '/gradedDoctor/WorkforceManagement', component: WorkforceManagement,
        meta: {title: '排班管理'},
      },
    ]
  },
  {
    path: '/gradedNurse',
    component: Layout,
    meta: {title: '分级护士'},
    redirect: '/gradedNurse/Comprehensive',
    children: [
      {
        path: '/gradedNurse/Comprehensive', component: Comprehensive ,
        meta: {title: '综合列表'},
      },
      {
        path: '/gradedNurse/Modify', component: Modify,
        meta: {title: '修改个人信息'},
      },
      {
        path: '/gradedNurse/Work', component: Work,
        meta: {title: '排班管理'},
      },
    ]
  },
   {
    path: '/hospitalGeneral',
    component: Layout,
    meta: {title: '医院综合'},
    redirect: '/hospitalGeneral/RoomAdd',
    children: [
      {
        path: '/hospitalGeneral/RoomAdd', component: RoomAdd ,
        meta: {title: '新增科室'},
        
      },
      {
        path: '/hospitalGeneral/RoomAdministration', component: RoomAdministration,
        meta: {title: '科室管理'},
      }
    ]
  },
  {
    path: '/medicalDrugs',
    component: Layout,
    meta: {title: '医疗药品出入库'},
    redirect: '/medicalDrugs/AddDrugs',
    children: [
      {
        path: '/medicalDrugs/AddDrugs', component: AddDrugs ,
        meta: {title: '添加药品耗材'},
      },
      {
        path: '/medicalDrugs/Integrated', component: Integrated,
        meta: {title: '医疗品综合管理'},
      }
    ]
  },
  {
    path: '/medicalFront',
    component: Layout,
    meta: {title: '医疗前线'},
    redirect: '/medicalFront/PrescriptionAdministration',
    children: [
      {
        path: '/medicalFront/PrescriptionAdministration', component: PrescriptionAdministration,
        meta: {title: '处方开具'},
      },
      {
        path: '/medicalFront/PrescriptionLssued', component: PrescriptionLssued,
        meta: {title: '处方综合管理'},
      }
    ]
  },
  {
    path: '/PatientData',
    component: Layout,
    meta: {title: '患者大数据'},
    redirect: '/PatientData/Consume',
    children: [
      {
        path: '/PatientData/Consume', component: Consume ,
        meta: {title: '药品耗材'},
      },
      {
        path: '/PatientData/Disease', component: Disease,
        meta: {title: '疾病统计'},
      }
    ]
  },
  {
    path: '/pharmacy',
    component: Layout,
    meta: {title: '中西医药房'},
    redirect: '/pharmacy/Look',
    children: [
      {
        path: '/pharmacy/Prescription', component: Prescription,
        meta: {title: '处方综合管理'},
      },
      {
        path: '/pharmacy/RevenueDataForm', component: RevenueDataForm,
        meta: {title: '营收数据表格'},
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
