import request from '@/utils/request'

//医疗药品
//获取医疗药品列表
export const $_drugsList = params => {
    return request({
        method: 'get',
        url: '/drugs/list',
        params
    })
}

//获取单条数据
export const $_drugsDel = params => {
    return request({
        method: 'get',
        url: '/drugs/del',
        params
    })
}

//添加医疗药品
export const $_drugsAdd = data =>{
    return request({
        method:'post',
        url:'/drugs/add',
        data
    })
}

// 医疗药品多条件搜索查询
export const $_drugsSearch= params => {
    return request({
        method: 'get',
        url: '/drugs/search',
        params
    })
}

// 查询所有医疗品分类
export const $_allDrugs= params => {
    return request({
        method: 'get',
        url: '/drugs/drugstype',
        params
    })
}

//批量删除药品
export const $_batchdel = data =>{
    return request({
        method:'post',
        url:'/drugs/batchdel',
        data
    })
}
