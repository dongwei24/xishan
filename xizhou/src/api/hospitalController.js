import request from '@/utils/request'

//添加科室
export const $_departmentAdd= params => {
    return request({
        method: 'get',
        url: '/department/add',
        params
    })
}

// 获取科室列表
export const $_departmentList= params => {
    return request({
        method: 'get',
        url: '/department/list',
        params
    })
}

// 根据科室名称查询数据列表
export const $_searchdate= params => {
    return request({
        method: 'get',
        url: '/department/search',
        params
    })
}

// 删除科室
export const $_departmentDel= params => {
    return request({
        method: 'get',
        url: '/department/del',
        params
    })
}

// 批量删除科室 
export const $_batchdel= params => {
    return request({
        method: 'get',
        url: '/department/batchdel',
        params
    })
}

// 修改科室数据
export const $_hanldEdit= params => {
    return request({
        method: 'get',
        url: '/department/edit',
        params
    })
}