export var checkAccount = (rule, value, callback) => {
    if (!value) {
        return callback(new Error('请输入用户名'))
    }
    if (!(/^[\u4e00-\u9fa5a-zA-Z0-9_]{3,6}$/.test(value))) {
        return callback(new Error('请输入正确的用户名'))
    }
    callback();
}
// 123

export var checkPwd = (rule, value, callback) => {
    if (!value) {
        return callback(new Error('请输入密码'))
    }
    if (!(/^\w{3,6}$/.test(value))) {
        return callback(new Error('请输入正确的密码'))
    }
    callback();
}

//时间戳转换为时间
export let getTimer = ()=>{  
        let time = new Date();
        let Y = fillZero(time.getFullYear())
        let M = fillZero(time.getMonth())
        let D = fillZero(time.getDate())
        let h = fillZero(time.getHours())
        let m = fillZero(time.getMinutes())
        let s = fillZero(time.getSeconds())
        return  Y+"年"+M+"月"+D+"日"+h +"时"+ m+"分"+s+"秒";
    }
    let fillZero = (t) => {
        return t<10?"0"+t:t
    }