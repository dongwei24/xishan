import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'


Vue.config.productionTip = false

//引入element ui
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

//引入公共样式
import '@/assets/css/normalize.css'
import '@/assets/css/reset.css'

// 导入echarts可视化图表工具
import * as echarts from 'echarts';
Vue.prototype.$echarts = echarts;


Vue.prototype.$bus = new Vue()

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
